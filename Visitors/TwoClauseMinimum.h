#ifndef LLVM_CLANG_TOOLS_EXTRA_SWITCH_CHECK_VISITORS_TWO_CLAUSE_MINIMUM_H
#define LLVM_CLANG_TOOLS_EXTRA_SWITCH_CHECK_VISITORS_TWO_CLAUSE_MINIMUM_H

#include "clang/AST/RecursiveASTVisitor.h"

namespace clang {
namespace autosar {

class TwoClauseMinimumVisitor
    : public RecursiveASTVisitor<TwoClauseMinimumVisitor> {
  DiagnosticsEngine &DE;
  unsigned DiagID;

  static const StringRef CheckName;
  static const StringRef Description;

public:
  TwoClauseMinimumVisitor(DiagnosticsEngine &DE);

  bool VisitSwitchStmt(const SwitchStmt *SS);
};

} // namespace autosar
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_SWITCH_CHECK_VISITORS_TWO_CLAUSE_MINIMUM_H