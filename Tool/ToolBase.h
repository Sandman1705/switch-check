#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Tooling/Tooling.h"

namespace clang {
namespace autosar {

class AutosarContext {
public:
  void setDiagnosticsEngine(DiagnosticsEngine *DiagEngine) {
    this->DiagEngine = DiagEngine;
  }

  DiagnosticsEngine *getDiagnosticsEngine() { return DiagEngine; }

  void setSourceManager(SourceManager *SourceMgr) {
    DiagEngine->setSourceManager(SourceMgr);
  }

  void setCompilerInstance(CompilerInstance *CI) { this->CI = CI; }

  CompilerInstance *getCompilerInstance() { return CI; }

  bool IgnoreIncludes = false;
  bool IgnoreSystemIncludes = true;

  bool VWellFormed = false;       // M6-4-3 visitor
  bool MWellFormed = false;       // M6-4-3 matcher
  bool VStructuredLabel = false;  // M6-4-4 visitor
  bool MStructuredLabel = false;  // M6-4-4 matcher
  bool VTerminateClause = false;  // M6-4-5 visitor
  bool MTerminateClause = false;  // M6-4-5 matcher
  bool VDefaultFinal = false;     // M6-4-6 visitor
  bool MDefaultFinal = false;     // M6-4-6 matcher
  bool VNoBoolCondition = false;  // M6-4-7 visitor
  bool MNoBoolCondition = false;  // M6-4-7 matcher
  bool VTwoClauseMinimum = false; // A6-4-1 visitor
  bool MTwoClauseMinimum = false; // A6-4-1 matcher

  void setSkipTemplated(bool skip) { SkipTemplated = skip; }

  bool getSkipTemplated() { return SkipTemplated; }

private:
  DiagnosticsEngine *DiagEngine;
  CompilerInstance *CI;

  bool SkipTemplated = true;
};

class AutosarConsumer : public ASTConsumer {
public:
  AutosarConsumer(AutosarContext &Context)
      : Context(Context) {}

  ~AutosarConsumer() override {}
  void HandleTranslationUnit(ASTContext &AC) override;
  bool HandleTopLevelDecl(DeclGroupRef DG) override;

private:
  AutosarContext &Context;
  std::vector<Decl *> TopLevelDecls;

  void runVisitors(ASTContext &AC, DiagnosticsEngine &DE);
  void runMatchers(ASTContext &AC, DiagnosticsEngine &DE);
};

class AutosarAction : public SyntaxOnlyAction {
public:
  AutosarAction(AutosarContext &Context) : Context(Context) {}

protected:
  std::unique_ptr<clang::ASTConsumer>
  CreateASTConsumer(CompilerInstance &CI, StringRef InFile) override {
    Context.setCompilerInstance(&CI);
    // Suppress default clang diagnostics
    CI.getDiagnostics().setSuppressAllDiagnostics(true);

    return std::make_unique<AutosarConsumer>(Context);
  }

private:
  AutosarContext &Context;
};

class AutosarFrontendActionFactory : public tooling::FrontendActionFactory {
public:
  AutosarFrontendActionFactory(AutosarContext &Context) : Context(Context) {}

  std::unique_ptr<FrontendAction> create() override {
    return std::make_unique<AutosarAction>(Context);
  }

private:
  AutosarContext &Context;
};

} // namespace autosar
} // namespace clang
