#include "NoBoolCondition.h"
#include "../Common/Diags.h"

namespace clang {
namespace autosar {

const StringRef NoBoolConditionVisitor::CheckName =
    "M6-4-7 NoBoolConditionVisitor";
const StringRef NoBoolConditionVisitor::Description =
    "The condition of a switch statement shall not have bool type";

NoBoolConditionVisitor::NoBoolConditionVisitor(DiagnosticsEngine &DE)
    : DE(DE), DiagID(getNewDiagID(DE, CheckName, Description)) {}

bool NoBoolConditionVisitor::VisitSwitchStmt(const SwitchStmt *DS) {
  const Expr *Cond = DS->getCond();

  if (Cond->isKnownToHaveBooleanValue()) {
    DE.Report(Cond->getBeginLoc(), DiagID) << Cond->getSourceRange();
  }

  return true;
}

} // namespace autosar
} // namespace clang
