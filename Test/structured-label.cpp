// RUN: switch-check --v-structured-label %s -- 2>&1 | FileCheck %s
// RUN: switch-check --m-structured-label %s -- 2>&1 | FileCheck %s

void f() {}

void test() {

  int x;
  switch (x) {
  case 1:
    if (true) {
    case 2: // CHECK: [[@LINE]]:5: warning: A switch-label shall only be used when the most closely-enclosing compound statement is the body of a switch statement [M6-4-4 StructuredLabel{{Visitor|Matcher}}]
      f();
    }
    break;
  default:
    break;
  }
}

void test2() {

  int x;
  switch (x) {
  case 1:
    break;
  case 2:
    f();
    break;
  if (true) {
    case 3: // CHECK: [[@LINE]]:5: warning: A switch-label shall only be used when the most closely-enclosing compound statement is the body of a switch statement [M6-4-4 StructuredLabel{{Visitor|Matcher}}]
    // cases 4-7 are ignored as they are part of fallthrough group and repeating
    // warnings here will not be useful for the user.
    case 4:
    case 5:
    case 6:
    case 7: {
      break;
    }
  }
  default:
    break;
  }
}

void test3() {
  int x;
  switch (x) {
  case 10: {
    switch (x) {
    case 1:
      break;
    case 2:
      break;
    default:
      break;
    }
    case 20: // CHECK: [[@LINE]]:5: warning: A switch-label shall only be used when the most closely-enclosing compound statement is the body of a switch statement [M6-4-4 StructuredLabel{{Visitor|Matcher}}]
      break;
  }
  case 30:
    break;
  default:
    break;
  }
}

// CHECK: 3 warnings generated
