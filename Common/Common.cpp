
#include "Common.h"
#include "clang/AST/ExprCXX.h"

namespace clang {
namespace autosar {

const SwitchCase *getFallthroughCase(const SwitchCase *SC) {
  while (const SwitchCase *Fallthrough =
             dyn_cast_or_null<SwitchCase>(SC->getSubStmt()))
    SC = Fallthrough;
  return SC;
}

bool isBreakOrThrow(const Stmt *S) {
  return isa<CXXThrowExpr>(S) || isa<BreakStmt>(S);
}

bool isLastStmtTerminator(const Stmt *S) {
  if (isBreakOrThrow(S))
    return true;
  if (const CompoundStmt *CS = dyn_cast_or_null<CompoundStmt>(S))
    return isLastStmtTerminator(CS->body_back());
  return false;
}


} // namespace autosar
} // namespace clang
