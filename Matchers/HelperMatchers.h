#ifndef LLVM_CLANG_TOOLS_EXTRA_SWITCH_CHECK_MATCHERS_HELPERMATCHERS_H
#define LLVM_CLANG_TOOLS_EXTRA_SWITCH_CHECK_MATCHERS_HELPERMATCHERS_H

#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchersInternal.h"

namespace clang {
namespace autosar {
using namespace clang::ast_matchers;
using namespace clang::ast_matchers::internal;

/// Predicate used by afterBoundNode matcher to compare SourceLocations
struct AfterBoundNodePredicate {
  AfterBoundNodePredicate(SourceManager &SM) : SM(SM) {}

  static SourceLocation getBeginLoc(const clang::DynTypedNode &Node);
  bool operator()(const BoundNodesMap &Nodes) const;

  SourceManager &SM;
  std::string ID;
  clang::DynTypedNode Node;
};

/// True if this node is after node bound by ID (based on Nodes'
/// SourceLocations).
AST_POLYMORPHIC_MATCHER_P(afterBoundNode,
                          AST_POLYMORPHIC_SUPPORTED_TYPES(Decl, Stmt, Type,
                                                          QualType),
                          std::string, ID) {
  auto &SourceManager = Finder->getASTContext().getSourceManager();
  AfterBoundNodePredicate Predicate(SourceManager);
  Predicate.ID = ID;
  Predicate.Node = clang::DynTypedNode::create(Node);
  return Builder->removeBindings(Predicate);
}
} // namespace autosar
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_SWITCH_CHECK_COMMON_HELPERMATCHERS_H