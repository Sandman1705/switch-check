#include "StructuredLabel.h"
#include "../Common/Diags.h"
#include "clang/AST/ParentMapContext.h" // DynTypedNodeList

namespace clang {
namespace autosar {

const StringRef StructuredLabelVisitor::CheckName =
    "M6-4-4 StructuredLabelVisitor";
const StringRef StructuredLabelVisitor::Description =
    "A switch-label shall only be used when the most closely-enclosing "
    "compound statement is the body of a switch statement";

StructuredLabelVisitor::StructuredLabelVisitor(ASTContext &AC,
                                               DiagnosticsEngine &DE)
    : AC(AC), DE(DE), DiagID(getNewDiagID(DE, CheckName, Description)) {}

bool StructuredLabelVisitor::VisitSwitchCase(const SwitchCase *SC) {
  if (!isStructuredLabel(SC, AC))
    DE.Report(SC->getBeginLoc(), DiagID);

  return true;
}

bool StructuredLabelVisitor::isStructuredLabel(const SwitchCase *SC,
                                               ASTContext &AC) {
  // Get the parent of the SwitchCase.
  const DynTypedNodeList &Parents = AC.getParents(*SC);
  assert((Parents.size() > 0) && "At least one parent node expected.");

  // Ignore fallthrough case labels. Only one warning is reported per group.
  if (Parents[0].get<SwitchCase>())
    return true;

  // If this is a well structured label it's first enclosing parent is a
  // CompoundStmt which is the body of a SwitchStmt.
  if (const CompoundStmt *CS = Parents[0].get<CompoundStmt>()) {
    // Get the parent of the CompoundStmt.
    const DynTypedNodeList &CSParents = AC.getParents(*CS);
    assert((CSParents.size() > 0) && "At least one parent node expected.");

    if (CSParents[0].get<SwitchStmt>())
      return true;
  }

  return false;
}

} // namespace autosar
} // namespace clang
