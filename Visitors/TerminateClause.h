#ifndef LLVM_CLANG_TOOLS_EXTRA_SWITCH_CHECK_VISITORS_TERMINATE_CLAUSE_H
#define LLVM_CLANG_TOOLS_EXTRA_SWITCH_CHECK_VISITORS_TERMINATE_CLAUSE_H

#include "clang/AST/RecursiveASTVisitor.h"

namespace clang {
namespace autosar {

class TerminateClauseVisitor
    : public RecursiveASTVisitor<TerminateClauseVisitor> {
  DiagnosticsEngine &DE;
  unsigned DiagID;

  static const StringRef CheckName;
  static const StringRef Description;

public:
  TerminateClauseVisitor(DiagnosticsEngine &DE);

  bool VisitSwitchStmt(const SwitchStmt *SS);

  /// Check if this SwitchCase is terminated by a break or a throw.
  static bool
  isSwitchClauseTerminated(const SwitchCase *SC,
                           const CompoundStmt::const_body_iterator Curr,
                           const CompoundStmt::const_body_iterator End);
};

} // namespace autosar
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_SWITCH_CHECK_VISITORS_TERMINATE_CLAUSE_H