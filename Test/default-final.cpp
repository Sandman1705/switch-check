// RUN: switch-check --v-default-final %s -- 2>&1 | FileCheck %s
// RUN: switch-check --m-default-final %s -- 2>&1 | FileCheck %s

void test() {
  int x;

  switch (x)
  {
  default: // CHECK: [[@LINE]]:3: warning: The final clause of a switch statement shall be the default-clause [M6-4-6 DefaultFinal{{Visitor|Matcher}}]
  case 0:
    break;
  case 1:
  case 2:
    break;
  }

  switch (x)
  {
  case 0:
    break;
  case 1:
  case 2:
    break;
  default:
    break;
  }

  switch (x) // CHECK: [[@LINE]]:3: warning: The final clause of a switch statement shall be the default-clause [M6-4-6 DefaultFinal{{Visitor|Matcher}}]
  {
  case 0:
    break;
  case 1:
  case 2:
    break;
  }

  switch (x) // CHECK: [[@LINE]]:3: warning: The final clause of a switch statement shall be the default-clause [M6-4-6 DefaultFinal{{Visitor|Matcher}}]
  {
  case 0:
    break;
  case 1: {
    switch (x) {
    case 11:
      break;
    default:
      break;
    }
  }
  case 2:
    break;
  }

  switch (x)
  {
  default: { // CHECK: [[@LINE]]:3: warning: The final clause of a switch statement shall be the default-clause [M6-4-6 DefaultFinal{{Visitor|Matcher}}]
    switch (x) {
    case 11:
      break;
    default:
      break;
    }
  }
  case 0:
    break;
  case 1:
    break;
  }

  switch (x) // CHECK-DAG: [[@LINE]]:3: warning: The final clause of a switch statement shall be the default-clause [M6-4-6 DefaultFinal{{Visitor|Matcher}}]
  {
  case 0:
    break;
  case 1: {
    switch (x) { // CHECK-DAG: [[@LINE]]:5: warning: The final clause of a switch statement shall be the default-clause [M6-4-6 DefaultFinal{{Visitor|Matcher}}]
      case 11:
        break;
    }
  }
  case 2:
    break;
  }
}

// CHECK: 6 warnings generated
