#include "WellFormed.h"
#include "../Common/Diags.h"
#include "DefaultFinal.h"
#include "StructuredLabel.h"
#include "TerminateClause.h"

namespace clang {
namespace autosar {
using namespace clang::ast_matchers;
using namespace clang::ast_matchers::internal;

const StringRef WellFormedMatcher::Callback::CheckName =
    "M6-4-3 WellFormedMatcher";
const StringRef WellFormedMatcher::Callback::Description =
    "A switch statement shall be a well-formed switch statement";

const StringRef WellFormedMatcher::Callback::WarnSwitchStart =
    "Switch statement should start with a switch case";
const StringRef WellFormedMatcher::Callback::WarnDefaultFinal =
    "Default case should be last";
const StringRef WellFormedMatcher::Callback::WarnTerminateClause =
    "Case clause should end with a break/throw statement or a compound "
    "statement that ends in break/throw.";
const StringRef WellFormedMatcher::Callback::WarnStructuredLabel =
    "There should be no case clause inside a case block";

/// Match first Stmt from this code block (CompoundStmt)
AST_MATCHER_P(CompoundStmt, hasFirstStmt, Matcher<Stmt>, InnerMatcher) {
  return Node.body_front() &&
         InnerMatcher.matches(*Node.body_front(), Finder, Builder);
}

WellFormedMatcher::Callback::Callback(DiagnosticsEngine &DE)
    : DE(DE), DiagIDSwitchStart(getNewDiagID(DE, CheckName, WarnSwitchStart)),
      DiagIDDefaultFinal(getNewDiagID(DE, CheckName, WarnDefaultFinal)),
      DiagIDTerminateClause(getNewDiagID(DE, CheckName, WarnTerminateClause)),
      DiagIDStructuredLabel(getNewDiagID(DE, CheckName, WarnStructuredLabel)) {}

void WellFormedMatcher::Callback::run(const MatchFinder::MatchResult &Result) {
  const BoundNodes &BN = Result.Nodes;

  // Report a warning if the first Stmt in SwitchStmt is not a SwitchCase
  if (const Stmt *S = BN.getNodeAs<Stmt>("firstStmt"))
    DE.Report(S->getBeginLoc(), DiagIDSwitchStart);

  // Report a warning if SwitchCase is inside another SwitchCase's case block.
  if (const SwitchCase *SC = BN.getNodeAs<SwitchCase>("unstructured"))
    DE.Report(SC->getBeginLoc(), DiagIDStructuredLabel);

  // If default is not the last SwitchCase then report a warning pointing to
  // that DefaultStmt.
  if (const DefaultStmt *DS = BN.getNodeAs<DefaultStmt>("default"))
    DE.Report(DS->getBeginLoc(), DiagIDDefaultFinal);
  // If there is no DefaultStmt then report a warning pointing to the
  // SwitchStmt.
  else if (const SwitchStmt *SS = BN.getNodeAs<SwitchStmt>("switch"))
    DE.Report(SS->getBeginLoc(), DiagIDDefaultFinal);

  // If the last Stmt is terminating this SwitchCase is complaint.
  // And even if SwitchCase's SubStmt is terminating it's compliant only if
  // there is not a Stmt after it before next SwitchCase.
  if ((!BN.getNodeAs<Stmt>("terminating")) &&
      !(BN.getNodeAs<Stmt>("terminatingSub") &&
        BN.getNodeAs<SwitchCase>("laststmt"))) {
    if (const SwitchCase *SC = BN.getNodeAs<SwitchCase>("case"))
      DE.Report(SC->getBeginLoc(), DiagIDTerminateClause);
  }
}

StatementMatcher WellFormedMatcher::makeMatcher() {
  // Reuse the matcher from rule M6-4-4
  auto StructuredLabel = StructuredLabelMatcher::makeMatcher();

  // Reuse the matcher from rule M6-4-5
  auto TerminateClause = TerminateClauseMatcher::makeMatcher();

  // Reuse the matcher from rule M6-4-6
  auto DefaultFinal = DefaultFinalMatcher::makeMatcher();

  // Match SwitchStmt that has a CompoundStmt where first Stmt is not a
  // SwitchCase.
  auto StartWithCase = switchStmt(has(compoundStmt(
      hasFirstStmt(stmt(unless(switchCase())).bind("firstStmt")))));

  // Match for each of the different conditions.
  return stmt(
      eachOf(StructuredLabel, TerminateClause, DefaultFinal, StartWithCase));
}

} // namespace autosar
} // namespace clang
