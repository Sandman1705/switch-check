#ifndef LLVM_CLANG_TOOLS_EXTRA_SWITCH_CHECK_COMMON_DIAGS_H
#define LLVM_CLANG_TOOLS_EXTRA_SWITCH_CHECK_COMMON_DIAGS_H

#include "clang/Basic/Diagnostic.h"
#include "llvm/ADT/StringRef.h"

namespace clang {
namespace autosar {

unsigned getNewDiagID(DiagnosticsEngine &DE, const StringRef CheckName,
                      const StringRef Description,
                      DiagnosticIDs::Level Level = DiagnosticIDs::Warning);

} // namespace autosar
} // namespace clang

#endif // LLVM_CLANG_TOOLS_EXTRA_SWITCH_CHECK_COMMON_DIAGS_H